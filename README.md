# Learn Ultralight

基于[Ultralight框架](https://github.com/ultralight-ux/ultralight)，学习使用HTML5、CSS3、JS前端技术实现UI界面，使用C++处理后台数据，开发桌面应用程序。

## 1. 安装基本环境

先安装如下所示的git、xmake和Ultralight SDK三个包。

- [git](https://git-scm.com/)
- [xmake](https://xmake.io/#/), which is a modern C/C++ program build tool
- [Ultralight SDK](https://github.com/ultralight-ux/Ultralight/releases/)

Ultralight SDK安装配置：

- Ultralight SDK下载后，只需解压到特定的目录，如下图，这里解压到了“C:\ultralight-sdk”目录（你也可以根据自己的情况，解压到其它目录）。

![Ultralight_SDK解压结果.png](./docs/imgs/Ultralight_SDK解压结果.png)

- 然后将SDK的路径添加到系统环境变量“Ultralight_SDK”（需要新建）下即可。最终添加结果，如下图所示：

![Ultralight_SDK系统环境变量.png](./docs/imgs/Ultralight_SDK系统环境变量.png)

## 2. 克隆项目并编译

使用如下命令克隆项目的镜像文件

```shell
git clone https://gitee.com/xingrui94/learn_ultralight.git
```

使用如下命令进行编译：

```shell
cd learn_ultralight
xmake
```

## 3. 运行程序

双击各个子项目下"build/bin"目录下,上一步编译生成的的exe文件即可
