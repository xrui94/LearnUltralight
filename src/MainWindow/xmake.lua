-- ref：https://xmake.io/#/zh-cn/manual/project_target?id=target

-- 设置编译工具链
toolchain("mmsvc")
    set_kind("standalone")
    -- set_bindir("D:/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.29.30133/bin/Hostx64/x64")
    set_bindir("C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.31.31103/bin/Hostx64/x64")
    -- set_sdkdir("D:/Microsoft Visual Studio/2019/Community/VC/Tools/MSVC/14.29.30133")
    set_sdkdir("C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.31.31103")
toolchain_end()

-- 设置工程编译目标
target("testUltraLight")
    set_toolchains("mmsvc")

    -- 设置编译目标的类型
    set_kind("binary")
    set_plat(os.host())
    set_arch(os.arch())

    -- 设置编译生成目标的文件名
    -- set_filename("testUltraLight")
    -- set_prefixname("")
    -- set_suffixname("_v0.1.0")
    -- set_extension(".exe")

    -- 添加调试符号, 设置符号不可见
    -- set_symbols("debug", "hidden")

    -- 设置编译过程的优化级别（faster相当于-O2，fastest相当于-O3）
    set_optimize("faster")

    -- 设置语言标准(gnuxx17、cxx17)
    set_languages("cxx17")

    -- 设置浮点数的编译模式
    -- set_fpmodels("strict")

    -- 设置编译过程产生的文件、最终编译目标等的存放路径
    set_objectdir("$(scriptdir)/build/bin")
    set_dependir("$(scriptdir)/build/bin")
    set_targetdir("$(scriptdir)/build/bin")

    -- 添加要编译的源文件
    add_files("./src/*.cpp")

    -- 添加头文件搜索目录
    -- add_sysincludedirs() -- 提供类似gcc/clang的“-isystem”，msvc的“/external:I”，用以减少引入系统库头文件带来的警告
    add_includedirs("./src")
    add_includedirs("C:/ultralight-sdk/include")

    -- 添加静态库搜索目录和依赖的静态库
    add_linkdirs("C:/ultralight-sdk/lib")
    add_linkdirs("C:/Program Files (x86)/Windows Kits/10/Lib/10.0.19041.0/um/x64/")
    -- add_linkdirs("D:/Windows Kits/10/Lib/10.0.19041.0/um/x64/")

    add_links("UltralightCore", "AppCore", "Ultralight", "WebCore")

    -- ref: https://discourse.glfw.org/t/link-problem-when-compiling-from-terminal/979/6
    add_links("User32")                         -- msvc编译器，需要链接该静态库
    add_links("UserEnv")                        -- msvc编译器，需要链接该静态库
    add_links("Advapi32")                       -- msvc编译器，需要链接该静态库
    add_links("kernel32", "gdi32", "shell32")   -- msvc编译器，需要链接这些静态库
    -- add_syslinks("pthread", "dl")               -- 添加系统连接库，一般需要放到链接库的最后

    add_ldflags("-L/ENTRY:mainCRTStartup")   -- 添加链接选项

    -- add_cxflags("/NODEFAULTLIB:msvcrt.lib", {force = true})
    -- add_cxflags("/MD", {force = true})

    after_build(function (target)
        local target_path = path.absolute(target:targetdir())
        os.cp("$(projectdir)/src/ui/", path.join(target:targetdir(), "assets"), {symlink = true})   -- 将程序依赖的ui文件拷贝到目标目录下
        os.cp("$(env ULTRALIGHT_SDK)/bin/resources", target:targetdir())    -- 将程序依赖的SDK资源文件拷贝到目标目录下
        os.cp("$(env ULTRALIGHT_SDK)/bin/*.dll", target:targetdir())    -- 将程序依赖SDK的动态库拷贝到目标目录下
        os.rm(path.join(target:targetdir(), target:name()))   -- 删除编译过程产生的中间文件、依赖文件等
    end)
    

target_end()