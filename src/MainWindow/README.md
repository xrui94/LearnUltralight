# MainWindow

MainWindow是一个包含菜单栏、工具栏、状态栏、工作区、面板等常用组件的窗口程序。其基本运行结果如下图所示:

![app运行结果.png](./docs/imgs/app运行结果.png)
