
function mainWindow() {
    layui.use(['tree', 'util'], () => {
        let tree = layui.tree;
        let layer = layui.layer;
        let util = layui.util;
        //模拟数据1
        let data1 = [{
            title: '江西',
            id: 1,
            children: [
                {
                    title: '南昌',
                    id: 1000,
                    children: [{
                        title: '青山湖区',
                        id: 10001
                    },
                    {
                        title: '高新区',
                        id: 10002
                    }
                    ]
                },
                {
                    title: '九江',
                    id: 1001
                },
                {
                    title: '赣州',
                    id: 1002
                }]
        },
        {
            title: '广西',
            id: 2,
            children: [{
                title: '南宁',
                id: 2000
            },
            {
                title: '桂林',
                id: 2001
            }]
        },
        {
            title: '陕西',
            id: 3,
            children: [{
                title: '西安',
                id: 3000
            },
            {
                title: '延安',
                id: 3001
            }]
        }]
        //按钮事件
        util.event('lay-demo', {
            getChecked: function (othis) {
                var checkedData = tree.getChecked('demoId1'); //获取选中节点的数据

                layer.alert(JSON.stringify(checkedData), { shade: 0 });
                console.log(checkedData);
            }
            , setChecked: function () {
                tree.setChecked('demoId1', [12, 16]); //勾选指定节点
            }
            , reload: function () {
                //重载实例
                tree.reload('demoId1', {

                });

            }
        });

        //无连接线风格
        tree.render({
            elem: '#file-list-context'
            , data: data1
            , showLine: false  //是否开启连接线
        });

    });
}

function main() {
    mainWindow();
    document.ready = () => {
        // calling native method defined in main.cpp
        const thisWindow = Window.this;
        document.body.innerText = thisWindow.frame.nativeMessage();
        // alert('hello...');
    }
}

export {
    main
};